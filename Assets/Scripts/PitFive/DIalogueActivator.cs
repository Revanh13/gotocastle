using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DIalogueActivator : MonoBehaviour
{
    public List<GameObject> Dialogues = new List<GameObject>();
	private int count = 0;

    public void ActivateNextDialogue()
	{
		Dialogues[count].SetActive(true);
		count++;
	}
}
