using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
	public AudioManager audioManager;

	private void Start()
	{
		audioManager.Play("Theme");
	}

	public void SwitchScene()
	{
		SceneManager.LoadScene(1);
	}
}