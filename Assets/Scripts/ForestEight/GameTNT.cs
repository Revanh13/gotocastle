using System.Collections;
using UnityEngine;

public class GameTNT : MonoBehaviour
{
	public Transform player;
	public Transform Target;
	public Animator playerAnimator;
	public Animator GameAnimator;
	public Transform cameraTransform;
	public AudioManager audioManager;

	public float speed;

	public GameObject Markus;
	public GameObject Camera;
	public GameObject Fakel;

	private Vector3 startPosMarkus;
	private Vector3 startPosCamera;
	private Vector3 startPosFakel;

	private bool IsPlay = true;

	private void Start()
	{
		startPosMarkus = Markus.transform.position;
		startPosCamera = Camera.transform.position;
		startPosFakel = Fakel.transform.position;
	}

	private void Update()
	{
		if (IsPlay)
		{
			if (Vector3.Distance(player.position, Target.position) > 1f)
			{
				playerAnimator.SetTrigger("Walk");
				player.position = Vector3.Lerp(player.position, Target.position, speed * Time.deltaTime);
				Vector3 camPos = new Vector3(player.position.x, cameraTransform.position.y, cameraTransform.position.z);
				if (camPos.x < 42.06f)
					camPos.x = 42.06f;
				cameraTransform.position = Vector3.Lerp(cameraTransform.position, camPos, speed * Time.deltaTime);
			}
			else
			{
				playerAnimator.SetTrigger("Idle");
			}
		}
	}

	private void OnTriggerEnter2D(Collider2D collision)
	{
		if (collision.CompareTag("Player"))
		{
			audioManager.Stop("Sword");
			GameAnimator.SetTrigger("Start");
			IsPlay = false;
			Markus.transform.position = new Vector3(86f, -2.71f, 0f);
			playerAnimator.SetTrigger("Idle");
			cameraTransform.position = new Vector3(85.2f, -1f, -10);
			Fakel.SetActive(false);
		}
	}

	public void RestartGame()
	{
		Markus.transform.position = startPosMarkus;
		Camera.transform.position = startPosCamera;

		Fakel.SetActive(false);

		Fakel.transform.position = startPosFakel;
		StartCoroutine(FakelAct());
	}

	private IEnumerator FakelAct()
	{
		yield return new WaitForSeconds(1f);
		Fakel.SetActive(true);
	}
}