using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EightAnim : MonoBehaviour
{
    public List<GameObject> splashs = new List<GameObject>();
	private int count = 0;

    public void ActivateSPlash()
	{
		splashs[count].SetActive(true);
		count++;
	}
}
