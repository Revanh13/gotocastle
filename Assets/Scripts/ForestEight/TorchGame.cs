using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TorchGame : MonoBehaviour
{
	public GameTNT gameTNT;

	private void OnTriggerEnter2D(Collider2D collision)
	{
		if (collision.gameObject.CompareTag("Respawn"))
		{
			gameTNT.RestartGame();
		}
	}
}
