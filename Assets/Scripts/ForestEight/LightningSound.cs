using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightningSound : MonoBehaviour
{
    public AudioManager audioManager;

    public void PlayOne()
	{
		audioManager.Play("LightningOneShot");
	}

	public void PlayTwo()
	{
		audioManager.Play("LightningTwoShot");
	}
}
