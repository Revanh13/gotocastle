using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PlateCount : MonoBehaviour
{
    public int NeedCount;
    public int CurrentCount;

    public UnityEvent NeedCountReadyEvent;

    public void AddCount()
    {
        CurrentCount++;
        if (NeedCount == CurrentCount)
            NeedCountReadyEvent.Invoke();
    }
}
