using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeroForestOne : MonoBehaviour
{
    public GameObject dialoguePanel;
	public Animator anim;

    public void ActivateSplash()
	{
		anim.SetTrigger("Idle");
		dialoguePanel.SetActive(true);
	}
}
