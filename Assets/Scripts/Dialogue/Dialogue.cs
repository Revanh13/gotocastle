using System;
using UnityEngine;
using UnityEngine.Events;

[Serializable]
public class Dialogue
{
	public Sprite Icon;
	public string Header;
	public string Text;
	public UnityEvent OnActivate;
}