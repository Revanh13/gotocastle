using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class DialoguePanel : MonoBehaviour
{
    [Header("�������")]
    public List<Dialogue> Dialogues = new List<Dialogue>();

    [Header("UI")]
    public Image Icon;
    public Text Header;
    public Text MainText;

    [Header("���������")]
    public float textSpeed;

    [Header("�������")]
    public UnityEvent OnDisable;

    private bool isPress = false;

    private bool isTimeLeft = false;

    void Start()
    {
        StartCoroutine(DialogueWork());
        StartCoroutine(TimeSkip());
    }

	private void Update()
	{
        if (isTimeLeft)
		{
            if (Input.GetKeyDown(KeyCode.Mouse0) || Input.GetKeyDown(KeyCode.Mouse1) ||
                Input.GetKeyDown(KeyCode.Space) || Input.GetKeyDown(KeyCode.Return))
                isPress = true;
		}
    }

    private IEnumerator TimeSkip()
	{
        yield return new WaitForSeconds(1f);
        isTimeLeft = true;
	}

	private IEnumerator DialogueWork()
	{
		if (Dialogues.Count == 0)
			yield return null;

		for (int i = 0; i < Dialogues.Count; i++)
		{
            SetDialogue(Dialogues[i]);
            yield return new WaitUntil(() => isPress == true);
            isPress = false;
		}

        OnDisable.Invoke();

        gameObject.SetActive(false);
	}

    private void SetDialogue(Dialogue dialogue)
	{
        Icon.sprite = dialogue.Icon;
        Header.text = dialogue.Header;
        MainText.text = dialogue.Text;
        dialogue.OnActivate.Invoke();
	}
}
