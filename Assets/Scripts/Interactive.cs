using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

[Serializable]
public class ScreenVec4
{
    public int LeftLine;
    public int RightLine;
    public int UpperLine;
    public int ButtomLine;

    public bool IsInclude(Vector3 pos)
    {
        bool res = true;
        if (pos.x < LeftLine || pos.x > RightLine) res = false;
        if (pos.y < ButtomLine || pos.y > UpperLine) res = false;
        return res;
    }
}

public class Interactive : MonoBehaviour
{
    public bool IsPeretaskivaetsa;
    public bool IsNeedToUseOnSmth;
    public bool IsNeedToPlace;

    public string TargetUsableObjectName = "�������";
    public ScreenVec4 GroundPlacingBorders;

    public UnityEvent TargetObjectOnUseEvent;
    public UnityEvent OnClickEvent;

    public float ScalingValue = 1.2f;

    private bool isDrag;
    private Vector3 baseScale;

    private GameObject thisObject;


    private void Start()
    {
        InitParams();
        SetupPlacingBorders();

    }

    private void InitParams()
    {
        isDrag = false;
        thisObject = gameObject;
        baseScale = thisObject.transform.localScale;
    }

    private void SetupPlacingBorders()
    {
        GroundPlacingBorders.LeftLine = 10;
        GroundPlacingBorders.RightLine = Screen.width - 10;
        GroundPlacingBorders.UpperLine = Screen.height - 20;
        GroundPlacingBorders.ButtomLine = 10;
    }

    private void OnMouseDrag()
    {
        isDrag = true;
        thisObject.transform.localScale = baseScale * ScalingValue;

        Vector3 res = Camera.main.ScreenToWorldPoint(Input.mousePosition);

        res.z = -1f;

        //����� ������ �� �����
        if (IsPeretaskivaetsa)
            thisObject.transform.localPosition = Vector3.Lerp(thisObject.transform.localPosition, res, 0.4f);
    }

	private void OnMouseDown()
	{
		OnClickEvent.Invoke();
	}

	private void OnMouseUp()
    {
        if (isDrag != true)
        {
            //��������� �����
            //OnClickEvent.Invoke();
        }
        else
        {
            //���������� �/��� ��������� ������������ �������
            if (IsNeedToUseOnSmth) UseObjectOnSmth();
            if (IsNeedToPlace) PlaceObject();
            thisObject.transform.localScale = baseScale;
        }

        isDrag = false;
    }

    private void PlaceObject()
    {
        //�������� ���������� ������

        Vector3 pos = thisObject.transform.position;
        if (GroundPlacingBorders.IsInclude(pos))
        {
            //��������� Image ���, �� � �������� ������

        }
        else
        {
            //������� � ������ ������ (����� ������� � ��������� ������
            //thisObject.transform.localPosition = Vector3.Lerp(thisObject.transform.localPosition, new Vector3(Screen.width, Screen.height) / 2f, 1f);

        }
    }

    private void UseObjectOnSmth()
    {
        //�������� ������������ ������ �� ������ ������ ����
        if (Physics.Raycast(thisObject.transform.position, Vector3.forward, out RaycastHit backObject, 10f))
        {
            if (backObject.collider.gameObject.name == TargetUsableObjectName)
            {
                //���� ������ ������ ������ ������������� ������� �������� ������ - �������� ��� UnityEvent
                TargetObjectOnUseEvent.Invoke();
            }
            else print("�� ��� ������");
        }
        else print("�� ����� ������");
    }
}
