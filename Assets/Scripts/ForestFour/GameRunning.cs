using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameRunning : MonoBehaviour
{
    public Animator MarkusAnim;

    public GameObject Markus;
    public GameObject Medved;
    public GameObject Camera;

    public List<GameObject> Stones = new List<GameObject>();

    public bool IsPlay;

    public float speed;

    private List<Vector3> stones = new List<Vector3>();

    private Vector3 startPosMarkus;
    private Vector3 startPosMedved;
    private Vector3 startPosCamera;

    // Start is called before the first frame update
    void Start()
    {
        startPosMarkus = Markus.transform.position;
        startPosMedved = Medved.transform.position;
        startPosCamera = Camera.transform.position;

        for (int i = 0; i < Stones.Count; i++)
		{
            var buf = Stones[i].transform.position;
            stones.Add(buf);
		}

        IsPlay = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (IsPlay)
		{
            Markus.transform.position = Vector3.Lerp(Markus.transform.position,
                            new Vector3(Markus.transform.position.x + 1, Markus.transform.position.y, Markus.transform.position.z),
                            speed * Time.deltaTime);

            Medved.transform.position = Vector3.Lerp(Medved.transform.position,
                            new Vector3(Medved.transform.position.x + 1, Medved.transform.position.y, Medved.transform.position.z),
                            speed * Time.deltaTime);
            Camera.transform.position = Vector3.Lerp(Camera.transform.position,
                            new Vector3(Camera.transform.position.x + 1, Camera.transform.position.y, Camera.transform.position.z),
                            speed * Time.deltaTime);
            if (Camera.transform.position.x >= 109)
			{
                IsPlay = false;
                MarkusAnim.SetTrigger("FallInHole");
			}
        }
    }

    public void RestartGame()
	{
        Markus.transform.position = startPosMarkus;
        Medved.transform.position = startPosMedved;
        Camera.transform.position = startPosCamera;

        for (int i = 0; i < Stones.Count; i++)
		{
            Stones[i].transform.position = stones[i];
            Stones[i].SetActive(true);
		}

        IsPlay = true;
	}
}
