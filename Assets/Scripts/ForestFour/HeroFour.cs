using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeroFour : MonoBehaviour
{
    public GameRunning gameRunning;

	private void OnCollisionEnter2D(Collision2D collision)
	{
		if (collision.gameObject.CompareTag("Respawn"))
		{
			gameRunning.RestartGame();
		}
	}
}
