using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Musorka : MonoBehaviour
{
	private void OnCollisionEnter2D(Collision2D collision)
	{
		if (collision.gameObject.CompareTag("Respawn"))
		{
			collision.gameObject.SetActive(false);
		}
	}
}
