using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class SplashScreenMainMenu : MonoBehaviour
{
	public Image SplashImage;
	public float TimeToFade = 2f;
	public Text BindText;
	public List<string> Texts = new List<string>();
	public UnityEvent Event;

	private void Start()
	{
		GoToFade();
	}

	private IEnumerator Fade(Image target, bool isAction = false)
	{
		float time = Time.time + TimeToFade; // ����� ���������� ��������
		float delta = target.color.a == 0 ? 0.01f : -0.01f;
		while (Time.time < time)
		{
			yield return new WaitForSeconds(0.01f);
			target.color = new Color(target.color.r, target.color.g, target.color.b, target.color.a + delta);
		}
		if (isAction)
		{
			Event.Invoke();
		}
	}

	private IEnumerator Fade(Text target, bool isAction = false)
	{
		float time = Time.time + TimeToFade; // ����� ���������� ��������
		float delta = target.color.a < 0.5f ? 0.01f : -0.01f;
		while (Time.time < time)
		{
			yield return new WaitForSeconds(0.01f);
			target.color = new Color(target.color.r, target.color.g, target.color.b, target.color.a + delta);
		}
		if (isAction)
		{
			Event.Invoke();
		}
	}

	public void GoToFade()
	{
		StartCoroutine(SwitchText());
	}

	private IEnumerator SwitchText()
	{
		yield return Fade(SplashImage);

		for (int i = 0; i < Texts.Count; i++)
		{
			BindText.text = Texts[i];
			yield return Fade(BindText);
			yield return Fade(BindText);
		}

		Event.Invoke();
	}
}