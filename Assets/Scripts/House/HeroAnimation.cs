using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeroAnimation : MonoBehaviour
{
    public GameObject DialoguePanel;
    private Animator animator;

    void Start()
    {
        animator = GetComponent<Animator>();
    }

    public void OnAnimationEnd()
	{
        DialoguePanel.SetActive(true);
	}
}
