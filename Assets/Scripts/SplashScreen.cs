using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class SplashScreen : MonoBehaviour
{
	public Image SplashImage;
	public float TimeToFade = 2f;
	public Text BindText;
	public List<string> Texts = new List<string>();
	public UnityEvent Event;
	public UnityEvent EventWithSecond;

	[Header("���� �� ���������")]
	public bool isAway;

	private void Start()
	{
		if (!isAway)
			StartCoroutine(GoToFade());
		else
			StartCoroutine(GoToFadeAndAway());
	}

	private IEnumerator Fade(Image target, bool isAction = false)
	{
		float time = Time.time + TimeToFade; // ����� ���������� ��������
		float delta = target.color.a < 0.5f ? 0.01f : -0.01f;
		while (Time.time < time)
		{
			yield return new WaitForSeconds(0.01f);
			target.color = new Color(target.color.r, target.color.g, target.color.b, target.color.a + delta);
		}
		if (isAction)
		{
			Event.Invoke();
		}
	}

	private IEnumerator Fade(Text target, bool isAction = false)
	{
		float time = Time.time + TimeToFade; // ����� ���������� ��������
		float delta = target.color.a < 0.5f ? 0.01f : -0.01f;
		while (Time.time < time)
		{
			yield return new WaitForSeconds(0.01f);
			target.color = new Color(target.color.r, target.color.g, target.color.b, target.color.a + delta);
		}
		if (isAction)
		{
			Event.Invoke();
		}
	}


	private IEnumerator GoToFade()
	{
		yield return StartCoroutine(SwitchText());
		gameObject.SetActive(false);
	}

	private IEnumerator GoToFadeAndAway()
	{
		yield return SwitchText(true);
		EventWithSecond.Invoke();
		gameObject.SetActive(false);
	}

	private IEnumerator SwitchText(bool isOut = false)
	{
		yield return Fade(SplashImage, true);

		if (BindText != null)
		{
			for (int i = 0; i < Texts.Count; i++)
			{
				BindText.text = Texts[i];
				yield return Fade(BindText);
				yield return Fade(BindText);
			}
		}

		if (isOut)
			yield return Fade(SplashImage);
	}
}