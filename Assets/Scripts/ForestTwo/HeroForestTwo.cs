using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeroForestTwo : MonoBehaviour
{
    public GameObject Doski;
	public GameObject DialogOnLuza;
	public GameObject Splash;
	public Animator anim;

    public void SlomatDoski()
	{
		Doski.SetActive(false);
		anim.SetTrigger("Fall");
	}

	public void SetUpDialog()
	{
		DialogOnLuza.SetActive(true);
	}

	public void SetUpSplash()
	{
		Splash.SetActive(true);
	}
}
